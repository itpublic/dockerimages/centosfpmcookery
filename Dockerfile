FROM centos:7

RUN localedef -i en_DK -f UTF-8 en_DK.UTF-8 \
 && echo "LANG=en_DK.UTF-8" > /etc/locale.conf \
 && export LANG=en_DK.UTF-8

RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash -
RUN yum -y install nodejs
RUN curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
RUN yum -y install yarn
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install \
    ansible \
    autoconf \
    automake \
    bison \
    gcc \
    gcc-c++ \
    git \
    libffi-devel \
    libtool \
    make \
    openssh-clients \
    openssl \
    openssl-devel \
    readline-devel \
    rpm-build \
    rsync \
    ruby \
    sqlite-devel \
    vim \
    wget \
    which \
    zlib-devel \
 && yum clean all

# Install RVM
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys \
      409B6B1796C275462A1703113804BB82D39DC0E3 \
      7D2BAF1CF37B13E2069D6956105BD0E739499BDB \
 && curl -sSL https://get.rvm.io | bash -s -- --autolibs=read-fail stable \
 && echo 'rvm_silence_path_mismatch_check_flag=1' >> ~/.rvmrc

SHELL ["/bin/bash", "-lc"]
CMD ["/bin/bash", "-l"]

# Install Ruby
RUN rvm install 2.6.7 \
 && rvm alias create 2.6 ruby-2.6.7 \
 && rvm use --default 2.6.7

ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN gem install -N fpm-cookery

RUN echo "export PATH=$PATH" > /etc/environment
